# VIAF Links Import

In VIAF there exist many links from the VIAF Record to Records from the source
datasets. Many of these links are indexed as schema:sameAs triples and can be 
found in the general VIAF Dataset. These published links focus on national 
libraries and other major institions.

The links we are interested in and are not represented in the data set is
the link to the RERO and Wikipedia pages.

These links can be found in the dump on http://viaf.org/viaf/data/ in the
`viaf-{YYYYMMDD}-links.txt.gz` textfile.

Each line represents a link between one VIAF URI and an external ID. The external
IDs are prefixed by the institution slug. 

This service filters these lines and transforms both RERO and WIKIPEDIA links
into small json documents. These documents can then be indexed in ElasticSearch.

