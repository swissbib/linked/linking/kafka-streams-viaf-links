/*
 * viaf links extraction
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import com.beust.klaxon.Klaxon
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataModel
import org.swissbib.types.EsBulkActions
import java.util.*

class KafkaTopology(private val properties: Properties, private val log: Logger) {

    private var countReroFound = 0

    fun build(): Topology {
        val builder = StreamsBuilder()
        builder.stream<String, SbMetadataModel>(properties.getProperty("kafka.topic.source"))
                .flatMap { _, value -> convertLine(value) }
                .to(properties.getProperty("kafka.topic.sink"))
        return builder.build()
    }

    private fun convertLine(value: SbMetadataModel): List<KeyValue<String, SbMetadataModel>> {
        val data = value.data
        val split = data.trim().split("\t")
        if (split.size != 2) {
            log.error("Could not parse line $data.")
            return listOf()
        }

        val viaf = split[0]
        val splitLinkedId = split[1].split("|", "@", limit = 2)
        if (splitLinkedId.size != 2) {
            log.error("Could not parse ID ${split[1]}.")
            return listOf()
        }
        val institution = splitLinkedId[0]
        val identifier = splitLinkedId[1]

        if (countReroFound % 1000 == 0) {
            log.info("Found a total of $countReroFound RERO IDs and indexed them in rero-viaf-links.")
        }
        return when (institution) {
            "RERO" -> {
                log.debug("Found RERO ID. Publish to rero-viaf-links!")
                countReroFound += 1
                val updatedIdentifier = identifier.replace("vtls", "http://data.rero.ch/02-A")
                listOf(KeyValue(updatedIdentifier,
                        SbMetadataModel()
                        .setData(Klaxon().toJsonString(mapOf(Pair("@id", updatedIdentifier), Pair("owl:sameAs", viaf))))
                        .setEsBulkAction(EsBulkActions.INDEX)
                        .setEsIndexName("rero-viaf-links-${value.messageDate}")
                ))
            }
            "Wikipedia" -> {
                log.debug("Found wikipedia link!")
                listOf(KeyValue(identifier,
                        SbMetadataModel()
                                .setData(Klaxon().toJsonString(mapOf(Pair("@id", identifier), Pair("foaf:primaryTopic", viaf))))
                                .setEsBulkAction(EsBulkActions.INDEX)
                                .setEsIndexName("wikipedia-viaf-links-${value.messageDate}")))
            }
            else -> {
                log.debug("Could not assign institution: $institution.")
                listOf()
            }
        }
    }
}
