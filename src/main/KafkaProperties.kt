/*
 * viaf links extraction
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.StreamsConfig
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataSerde
import java.io.File
import java.io.FileInputStream
import java.util.*
import kotlin.system.exitProcess

class KafkaProperties(
        private val log: Logger
) {
    val appProperties = loadAppProperties()
    val kafkaProperties: Properties = Properties()

    init {
        mapProperties()
    }


    private fun loadAppProperties(): Properties {
        val props = Properties()
        val file = File("/configs/app.properties")
        if (file.isFile)
            props.load(FileInputStream(file))
        else {
            props.load(ClassLoader.getSystemResourceAsStream("app.properties"))
            log.warn("Loading default appProperties from class path!")
        }
        return props
    }

    private fun mapProperties() {
        setProperty("application.id", abortIfMissing = true)
        setProperty("application.server")
        setProperty("bootstrap.servers", abortIfMissing = true)
        setProperty("buffered.records.per.partition")
        setProperty("cache.max.bytes.buffering")
        setProperty("client.id", abortIfMissing = true)
        setProperty("commit.interval.ms")
        setProperty("default.deserialization.exception.handler")
        setProperty("default.production.exception.handler")
        setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().javaClass)
        setProperty("metric.reporters")
        setProperty("metrics.num.samples")
        setProperty("metrics.sample.window.ms")
        setProperty("num.standby.replicas")
        setProperty("num.stream.threads")
        setProperty("partition.grouper")
        setProperty("processing.guarantee")
        setProperty("poll.ms")
        setProperty("replication.factor")
        setProperty("retries")
        setProperty("retry.backoff.ms")
        setProperty("state.cleanup.delay.ms")
        setProperty("state.dir")
        setProperty("timestamp.extractor")
        setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, SbMetadataSerde().javaClass)
        setProperty("windowstore.changelog.additional.retention.ms")

        setAppProperty("kafka.topic.source", abortIfMissing = true)
        setAppProperty("kafka.topic.sink", abortIfMissing = true)
    }


    private fun setAppProperty(propertyName: String, defaultValue: Any? = null, abortIfMissing: Boolean = false) {
        val envProperty = propertyName.replace("\\.".toRegex(), "_").toUpperCase()
        when {
            System.getenv(envProperty) != null -> {
                appProperties.setProperty(propertyName, System.getenv(envProperty))
                log.info("$propertyName=${appProperties.getProperty(propertyName)} (from env $envProperty)")
            }
            appProperties.getProperty(propertyName) != null -> {
                log.info("$propertyName=${appProperties.getProperty(propertyName)} (from app.properties)")
            }
            defaultValue != null -> {
                appProperties[propertyName] = defaultValue
                log.info("$propertyName=${appProperties.getProperty(propertyName)} (from default)")
            }
            abortIfMissing -> {
                log.error("Required property $propertyName not set! Aborting...")
                exitProcess(1)
            }
            else -> log.info("No value for $propertyName set.")
        }
    }

    private fun setProperty(propertyName: String, defaultValue: Any? = null, abortIfMissing: Boolean? = false) {
        val envProperty = propertyName.replace("\\.".toRegex(), "_").toUpperCase()
        when {
            System.getenv(envProperty) != null -> {
                log.trace("Found value for property $propertyName in environment variable $envProperty")
                kafkaProperties.setProperty(propertyName, System.getenv(envProperty))
            }
            appProperties.getProperty(propertyName) != null -> {
                log.trace("Found value for property $propertyName in appProperties file")
                kafkaProperties.setProperty(propertyName, appProperties.getProperty(propertyName))
            }
            defaultValue != null -> {
                kafkaProperties[propertyName] = defaultValue
            }
            abortIfMissing!! -> {
                log.error("Required property $propertyName not set! Aborting...")
                exitProcess(1)
            }
            else -> log.trace("No value for property $propertyName found")
        }
    }
}
